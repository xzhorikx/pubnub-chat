package com.alexz.pubnubchat.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.alexz.pubnubchat.R;

import java.util.List;

/**
 * Created by Alex on 22-Sep-17.
 */

public class AvatarAdapter extends ArrayAdapter<Drawable> {

    private List<Drawable> avatarList;
    private Context context;

    public AvatarAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Drawable> chatMessageList) {
        super(context, resource, chatMessageList);
        this.context = context;
        this.avatarList = chatMessageList;
    }
    @Override
    public void add(@Nullable Drawable avatar) {
        avatarList.add(avatar);
    }

    @Override
    public int getCount() {
        return avatarList.size();
    }

    @Nullable
    @Override
    public Drawable getItem(int position) {
        return avatarList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        Drawable avatar = getItem(position);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.adapter_avatar, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.ivAvatar);
        imageView.setImageDrawable(avatar);

        return view;
    }
}
