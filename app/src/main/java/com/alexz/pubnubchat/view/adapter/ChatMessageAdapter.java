package com.alexz.pubnubchat.view.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.model.ChatMessage;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.model.enums.Position;
import com.alexz.pubnubchat.presenter.helper.DateHelper;
import com.alexz.pubnubchat.presenter.helper.UserInfoHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 17-Sep-17.
 */

public class ChatMessageAdapter extends ArrayAdapter<ChatMessage> {

    private List<ChatMessage> chatMessageList;
    private Context context;

    public ChatMessageAdapter(@NonNull Context context, @LayoutRes int resource) {
        this(context, resource, new ArrayList<ChatMessage>());
    }

    public ChatMessageAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ChatMessage> chatMessageList) {
        super(context, resource, chatMessageList);
        this.context = context;
        this.chatMessageList = chatMessageList;
    }

    @Override
    public void add(@Nullable ChatMessage chatMessage) {
        chatMessageList.add(chatMessage);
    }

    @Override
    public int getCount() {
        return chatMessageList.size();
    }

    @Nullable
    @Override
    public ChatMessage getItem(int position) {
        return chatMessageList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        ChatMessage chatMessage = getItem(position);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Position pos = chatMessage.getPosition();

        switch (pos){
            case CENTER: {
                view = layoutInflater.inflate(R.layout.adapter_message_chat_wrapper_center, parent, false);
                break;
            }
            case LEFT: {
                view = layoutInflater.inflate(R.layout.adapter_message_chat_wrapper_left, parent, false);
                break;
            }
            case RIGHT:{
                view = layoutInflater.inflate(R.layout.adapter_message_chat_wrapper_right, parent, false);
                break;
            }
            default: view = layoutInflater.inflate(R.layout.adapter_message_chat_wrapper_left, parent, false);
        }
        if(pos != Position.CENTER) {
            ChatViewHolder viewHolder= new ChatViewHolder(view);
            view.setTag(viewHolder);

            setMessageBackground(viewHolder.messageChatContainer, chatMessage.getColorBg());

            viewHolder.tvSenderName.setText(chatMessage.getSenderName() + " id[" + chatMessage.getSenderId() + "]");
            viewHolder.tvMessage.setText(chatMessage.getMessage());
            if (chatMessage.getTimestamp() == 0) {
                viewHolder.tvMessage.setText(DateHelper.getTimeStampUtc());
            } else {
                viewHolder.tvMessageDate.setText(DateHelper.getTimeFromTimetoken(chatMessage.getTimestamp() / 10000));
            }
        } else{
            SystemChatViewHolder viewHolder = new SystemChatViewHolder(view);
            view.setTag(viewHolder);

            viewHolder.tvSystemTime.setText(DateHelper.getDateTimeFromTimetoken(chatMessage.getTimestamp() / 10000));
            viewHolder.tvSystemMessage.setText(chatMessage.getMessage());

        }

        return view;
    }

    private void setMessageBackground(LinearLayout messageChatContainer, Color colorBg) {

        int colorResourceId = UserInfoHelper.getColorResourceInt(colorBg);

        if(Build.VERSION.SDK_INT <= 21){
            messageChatContainer.setBackgroundColor(ContextCompat.getColor(context, colorResourceId));
        } else{
            ViewCompat.setBackgroundTintList(messageChatContainer, ContextCompat.getColorStateList(context, colorResourceId));
        }
    }

    static class ChatViewHolder{
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.tvSenderName)
        TextView tvSenderName;
        @BindView(R.id.messageChatContainer)
        LinearLayout messageChatContainer;
        @BindView(R.id.tvMessageDate)
        TextView tvMessageDate;

        public ChatViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class SystemChatViewHolder{

        @BindView(R.id.tvSystemTime)
        TextView tvSystemTime;
        @BindView(R.id.tvSystemMessage)
        TextView tvSystemMessage;
        public SystemChatViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public List<ChatMessage> getChatMessageList() {
        return chatMessageList;
    }
}
