package com.alexz.pubnubchat.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.presenter.helper.UserInfoHelper;

import java.util.List;

/**
 * Created by Alex on 22-Sep-17.
 */

public class ColorAdapter extends ArrayAdapter<Color> {
    private List<Color> colorList;
    private Context context;

    public ColorAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Color> colorList) {
        super(context, resource, colorList);
        this.context = context;
        this.colorList = colorList;
    }
    @Override
    public void add(@Nullable Color avatar) {
        colorList.add(avatar);
    }

    @Override
    public int getCount() {
        return colorList.size();
    }

    @Nullable
    @Override
    public Color getItem(int position) {
        return colorList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        Color color = getItem(position);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.adapter_color, null);

        TextView textView = (TextView) view.findViewById(R.id.tvColorChoose);
        int colorId = UserInfoHelper.getColorResourceInt(color);
        textView.setBackgroundResource(colorId);

        return view;
    }
}
