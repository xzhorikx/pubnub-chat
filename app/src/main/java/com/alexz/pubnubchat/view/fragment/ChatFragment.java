package com.alexz.pubnubchat.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alexz.pubnubchat.ChatApp;
import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.ChatMessage;
import com.alexz.pubnubchat.model.ChatMessageImpl;
import com.alexz.pubnubchat.model.enums.ChatMessageSource;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.model.enums.Position;
import com.alexz.pubnubchat.presenter.helper.PubNubHelper;
import com.alexz.pubnubchat.presenter.helper.UserInfoHelper;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;
import com.alexz.pubnubchat.view.adapter.ChatMessageAdapter;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.endpoints.History;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.history.PNHistoryResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 15-Sep-17.
 */

public class ChatFragment extends Fragment {

    private static final int LAYOUT = R.layout.fragment_chat;
    private static final String CHANNEL = "test_channel";

    private Context fragmentContext;
    private Set<Long> sentMessagesIdSet;

    @BindView(R.id.buttonSend)
    protected Button buttonSend;
    @BindView(R.id.etSend)
    protected EditText etSend;
    @BindView(android.R.id.list)
    protected ListView listViewMessages;

    protected ChatMessageAdapter chatMessageAdapter;

    private List<ChatMessage> dummyMessages;

    @Override
    public void onAttach(Context context) {
        Log.v("onView", "onAttach started");
        fragmentContext = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.v("onView", "onCreateView started");
        View view = inflater.inflate(LAYOUT, container, false);
        ButterKnife.bind(this, view);
        initElements();
        initListeners();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.v("onView", "onActivityCreated started");
        super.onActivityCreated(savedInstanceState);
        restoreListViewState();
    }

    @Override
    public void onPause() {
        Log.v("onView", "onPause started");
        super.onPause();
        cacheListViewState();
    }

    private PNCallback<PNHistoryResult> getHistoryCallback(final long lastSavedTimetoken){
        PNCallback<PNHistoryResult>  callback = new PNCallback<PNHistoryResult>() {
            @Override
            public void onResponse(PNHistoryResult result, PNStatus status) {
                if(!status.isError()) {
                    List<ChatMessage> chatFragmentList
                            = PubNubHelper.getChatMessageListFromHistory(result, ChatMessageImpl.class);
                    //TODO review this logic (end or start)
                    if (result.getEndTimetoken() > lastSavedTimetoken) {
                        UserPreferences.save
                                (Constants.LAST_SAVED_TIMETOKEN, result.getEndTimetoken());
                    }
                    UserPreferences.save(Constants.HISTORY_CACHE, chatFragmentList);
                    chatMessageAdapter.addAll(chatFragmentList);
                    chatMessageAdapter.notifyDataSetChanged();
                } else{
                    Log.e("Error_tag", "Error in history callback: " + status.getErrorData().getThrowable().getMessage());
                }
            }
        };
        return callback;
    }

    private void restoreListViewState() {
        restoreListViewFromCache();
        restoreListViewFromHistory();

    }

    private void restoreListViewFromCache() {
        List<ChatMessage> chatMessageList = UserPreferences.getCachedMessages();
        int index = UserPreferences.getInt(Constants.LIST_VIEW_INDEX);
        int top = UserPreferences.getInt(Constants.LIST_VIEW_TOP);

        chatMessageAdapter.clear();
        chatMessageAdapter.addAll(chatMessageList);
        chatMessageAdapter.notifyDataSetChanged();


        if(index!=-1)
            listViewMessages.setSelectionFromTop(index, top);
    }

    private void restoreListViewFromHistory() {
        long lastSavedTimetoken = UserPreferences.getLong(Constants.LAST_SAVED_TIMETOKEN);
        History history = ChatApp.getPubNub()
                .history()
                .channel(Constants.CHANNEL)
                .count(Constants.PUB_NUB_MSG_LIMIT);
        if(lastSavedTimetoken > 0){
            history.end(lastSavedTimetoken + 1);
        }
        history.includeTimetoken(true).async(getHistoryCallback(lastSavedTimetoken));
    }

    private void cacheListViewState() {
        List<ChatMessage> list = new ArrayList<>();

        // Limiting cache size to specific size
        int cacheSize =
                listViewMessages.getAdapter().getCount() > Constants.PUB_NUB_MSG_LIMIT
                        ? Constants.PUB_NUB_MSG_LIMIT
                        : listViewMessages.getAdapter().getCount();

        for (int i = listViewMessages.getAdapter().getCount() - cacheSize; i < listViewMessages.getAdapter().getCount(); i++) {
            list.add((ChatMessage) listViewMessages.getAdapter().getItem(i));
        }
        int index = listViewMessages.getFirstVisiblePosition();
        View v = listViewMessages.getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();

        UserPreferences.save(Constants.CACHED_MESSAGES_KEY, list);
        UserPreferences.save(Constants.LIST_VIEW_INDEX, index);
        UserPreferences.save(Constants.LIST_VIEW_TOP, top);

    }

    private void initElements() {
        sentMessagesIdSet = new HashSet<>();
        dummyMessages = new ArrayList<>();
        chatMessageAdapter = new ChatMessageAdapter(fragmentContext.getApplicationContext(), R.layout.adapter_message_chat_wrapper_right, dummyMessages);
        listViewMessages.setAdapter(chatMessageAdapter);
    }

    private void initListeners() {
        ChatApp.addPubNubCallback(getSubscribeListener());
        buttonSend.setOnClickListener(getSendButtonClickListener());
    }

    private SubscribeCallback getSubscribeListener() {
        SubscribeCallback callback = new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
                if (status.getCategory() == PNStatusCategory.PNConnectedCategory){
                    ChatMessage chatMessage = new ChatMessageImpl();
                    chatMessage.setSenderName("System");
                    chatMessage.setSenderId(Constants.SENDER_SYSTEM);
                    chatMessage.setMessage(UserPreferences.getStringFromPreferences(Constants.USER_NICKNAME) + " connected!");
                    chatMessage.setPosition(Position.CENTER);
                    sendMessage(chatMessage, ChatMessageSource.SYSTEM);
                }
            }

            @Override
            public void message(PubNub pubnub, final PNMessageResult message) {
                try {
                    Log.v("Verbose", message.toString());
                    UserPreferences.save(Constants.LAST_SAVED_TIMETOKEN, message.getTimetoken());
                    if(!isAlreadyDisplayed(message.getTimetoken())) {
                        ChatMessage chatMessage =
                                PubNubHelper.getChatMessageFromResult(message,
                                        ChatMessageImpl.class,
                                        UserPreferences.getStringFromPreferences(Constants.SENDER_ID));
                        displayMessage(chatMessage);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("Error_tag", "Error displaying message: " + e.toString());
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
                Log.v("Verbose", presence.toString());
            }
        };
        return callback;
    }

    private boolean isAlreadyDisplayed(Long timetoken){
        if(timetoken == 0){
            return false;
        } else if(sentMessagesIdSet.contains(timetoken)) {
            sentMessagesIdSet.remove(timetoken);
            return true;
        } else {
            return false;
        }
    }

    private void displayMessage(ChatMessage chatMessage) {
        try {
            chatMessageAdapter.add(chatMessage);
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e("Error_tag", "Unable to display message: " + e.toString());
        } finally {
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chatMessageAdapter.notifyDataSetChanged();
                    }
                });
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }

    }

    private void displayMessage(List<ChatMessage> chatFragmentList) {
        try{
            chatMessageAdapter.addAll(chatFragmentList);
        } catch (Exception e){
            e.printStackTrace();
            Log.e("Error_tag", "Unable to display list of messages: " + e.toString());
        } finally {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatMessageAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private View.OnClickListener getSendButtonClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatMessage chatMessage = getSendChatMessage();
                sendMessage(chatMessage, ChatMessageSource.USER);
            }
        };
        return listener;
    }

    private ChatMessage getSendChatMessage() {
        ChatMessage chatMessage = new ChatMessageImpl();
        chatMessage.setSenderId(UserPreferences.getStringFromPreferences(Constants.SENDER_ID));
        chatMessage.setSenderName(UserPreferences.getStringFromPreferences(Constants.USER_NICKNAME));
        chatMessage.setMessage(etSend.getText().toString());
        chatMessage.setPosition(Position.RIGHT);

        chatMessage.setColorBg(Color.getValue(UserPreferences.getInt(Constants.USER_COLOR)));
        return chatMessage;
    }

    private void sendMessage(ChatMessage chatMessage, ChatMessageSource source) {
        try{
            if(source == ChatMessageSource.USER)
                UserInfoHelper.incTotalMessageCount(1);
            ChatApp.getPubNub().publish()
                    .message(chatMessage)
                    .channel(CHANNEL)
                    .async(getPnCallback(chatMessage));
        } catch (Exception e){
            e.printStackTrace();
            Log.e("Error_tag", "Something went wrong while publishing message: "
                    + e.toString());
        }
    }

    private PNCallback<PNPublishResult> getPnCallback(final ChatMessage chatMessage) {
        PNCallback<PNPublishResult> pnCallback = new PNCallback<PNPublishResult>() {
            @Override
            public void onResponse(PNPublishResult result, PNStatus status) {
                try {
                    if(status.isError()){
                        Toast.makeText(fragmentContext.getApplicationContext(), "Error, can't send", Toast.LENGTH_LONG).show();
                        Log.e("Error_tag", status.toString());
                    } else {
                        chatMessage.setTimestamp(result.getTimetoken());
                        sentMessagesIdSet.add(result.getTimetoken());
                        clearTextInput();
                        Toast.makeText(fragmentContext.getApplicationContext(), "Successfully sent", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Error_tag", "Something went wrong while handling response: "
                            + e.toString());
                } finally {
                    displayMessage(chatMessage);
                }
            }
        };
        return pnCallback;
    }


    private void clearTextInput() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                etSend.getText().clear();
            }
        });
    }

}
