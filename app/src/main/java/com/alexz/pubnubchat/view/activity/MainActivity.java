package com.alexz.pubnubchat.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.presenter.helper.InputHelper;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;
import com.alexz.pubnubchat.view.fragment.ChatFragment;
import com.alexz.pubnubchat.view.fragment.PreferencesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 17-Sep-17.
 */

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation)
    NavigationView navigationView;
    private static final String CHAT_FRAGMENT_TAG = "chat_fragment";
    private static final String PREFERENCE_FRAGMENT_TAG = "pref_fragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppDefault);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        initToolbar();
        initDrawer();
        displayFragment();

    }

    private void displayFragment() {
        String fragmentTag = UserPreferences.getStringFromPreferences(Constants.LAST_FRAGMENT_TAG);
        if(fragmentTag.isEmpty() || fragmentTag.equals(CHAT_FRAGMENT_TAG)) {
            changeFragment(new ChatFragment(), CHAT_FRAGMENT_TAG);
        } else {
            changeFragment(new PreferencesFragment(), PREFERENCE_FRAGMENT_TAG);
        }
    }

    private void initToolbar() {
        toolbar.setTitle(R.string.app_name);
    }

    private void initDrawer() {
        // Adding 'hamburger icon'
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        drawerLayout.addDrawerListener(getDrawerListener(this));
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getDrawerItemClickListener());
    }

    private DrawerLayout.DrawerListener getDrawerListener(final Activity activity) {
        final DrawerLayout.DrawerListener listener= new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Hide soft keyboard when drawer is opened
                try{
                    InputHelper.hideKeyboard(activity);
                } catch (Exception e){
                    Log.e("Error_tag", "Unable to hide keyboard when drawer was opened. Exception: " + e.toString());
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        };
        return listener;
    }

    private NavigationView.OnNavigationItemSelectedListener getDrawerItemClickListener() {
        NavigationView.OnNavigationItemSelectedListener listener = new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                changeView(item);
                return true;
            }
        };
        return listener;
    }

    private void changeView(MenuItem item) {
        Fragment fragment = null;
        String tag = "";
        switch (item.getItemId()){
            case R.id.menu_chat:
                fragment = new ChatFragment();
                tag = CHAT_FRAGMENT_TAG;
                break;
            case R.id.menu_preferences:
                fragment = new PreferencesFragment();
                tag = PREFERENCE_FRAGMENT_TAG;
                break;
        }
        try {
            changeFragment(fragment, tag);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            drawerLayout.closeDrawers();
        }

    }

    private void changeFragment(Fragment fragment, String tag) {
        Fragment displayedFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if(displayedFragment == null || !displayedFragment.isVisible()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frameContent, fragment, tag).commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        UserPreferences.save(Constants.LAST_FRAGMENT_TAG, getDisplayedFragment());
    }

    protected String getDisplayedFragment(){
        Fragment displayedFragment = getSupportFragmentManager().findFragmentByTag(CHAT_FRAGMENT_TAG);
        if(displayedFragment == null || !displayedFragment.isVisible()) {
            return PREFERENCE_FRAGMENT_TAG;
        } else{
            return CHAT_FRAGMENT_TAG;
        }
    }
}
