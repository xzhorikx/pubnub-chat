package com.alexz.pubnubchat.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.User;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.presenter.helper.UserInfoHelper;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;
import com.alexz.pubnubchat.view.adapter.AvatarAdapter;
import com.alexz.pubnubchat.view.adapter.ColorAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 17-Sep-17.
 */

public class PreferencesFragment extends Fragment {

    private static final int LAYOUT = R.layout.fragment_preferences;
    private Context fragmentContext;
    private User user;
    private Dialog dialog;

    @BindView(R.id.tvNickname)
    TextView tvNickname;

    @BindView(R.id.ivAvatar)
    ImageView ivAvatar;

    @BindView(R.id.tvLevel)
    TextView tvLevel;

    @BindView(R.id.tvMessageCount)
    TextView tvMessageCount;

    @BindView(R.id.tvKarma)
    TextView tvKarma;


    @BindView(R.id.messageChatContainer)
    LinearLayout messageChatContainer;
    @BindView(R.id.tvSenderName)
    TextView tvSenderName;
    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @Override
    public void onAttach(Context context) {
        fragmentContext = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        setRetainInstance(true);
        ButterKnife.bind(this, view);
        user = User.build();
        initClickListeners();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewValues();
    }

    private void initClickListeners() {
        tvNickname.setOnClickListener(getNickClickListener());
        ivAvatar.setOnClickListener(getAvatarClickListener());
        messageChatContainer.setOnClickListener(getOnMessageClickListener());
    }

    private View.OnClickListener getOnMessageClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeColorDialog();
            }
        };
        return listener;
    }

    private View.OnClickListener getNickClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeNickDialog();
            }
        };
        return listener;
    }

    private View.OnClickListener getAvatarClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeAvatarDialog();
            }
        };
        return listener;
    }

    private void showChangeNickDialog() {
        View view = createDialog(R.layout.dialog_nickname);
        final EditText etNickname = (EditText) view.findViewById(R.id.etNickname);
        Button buttonSave = (Button) view.findViewById(R.id.buttonSave);

        etNickname.setText(UserPreferences.getStringFromPreferences(Constants.USER_NICKNAME));
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nickName = etNickname.getText().toString();
                UserPreferences.save(Constants.USER_NICKNAME, nickName);
                tvNickname.setText(nickName);
                tvSenderName.setText(nickName);
                dismissDialog();
            }
        });

        showDialog();
    }

    private void showChangeAvatarDialog() {
        Drawable[] drawable = getAllAvatars();
        int avatarCount = user.getLevel() < 10 ? (int) user.getLevel() : 10;
        int minimumAvailableAvatars = 3;

        // Make it minimum 3 available avatars
        avatarCount = avatarCount < minimumAvailableAvatars ? minimumAvailableAvatars : avatarCount;
        List<Drawable> drawableList = new ArrayList<>();
        for(int i = 0; i < avatarCount; i ++){
            drawableList.add(drawable[i]);
        }

        View view = createDialog(R.layout.dialog_avatars);
        AvatarAdapter avatarAdapter = new AvatarAdapter(fragmentContext, R.layout.adapter_avatar, drawableList);
        ListView listView = (ListView) view.findViewById(R.id.lvAvatar);
        listView.setAdapter(avatarAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserPreferences.save(Constants.USER_AVATAR, position);
                ivAvatar.setImageDrawable((Drawable) parent.getItemAtPosition(position));
                dismissDialog();
            }
        });

        showDialog();
    }

    private void showChangeColorDialog() {
        View view = createDialog(R.layout.dialog_color);
        Color[] colors = Color.values();
        ColorAdapter colorAdapter = new ColorAdapter(fragmentContext, R.layout.adapter_color, Arrays.asList(colors));
        ListView listView = (ListView) view.findViewById(R.id.lvColor);
        listView.setAdapter(colorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Color color = (Color) parent.getItemAtPosition(position);
                UserPreferences.save(Constants.USER_COLOR, color.getId());
                setMessageColor(UserInfoHelper.getColorResourceInt(color));
                dismissDialog();
            }
        });

        showDialog();
    }

    private Drawable[] getAllAvatars() {
        return new Drawable[]{ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_01),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_02),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_03),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_04),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_05),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_06),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_07),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_08),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_09),
                        ContextCompat.getDrawable(fragmentContext, R.mipmap.avatar_10)};
    }

    private View createDialog(int viewId) {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        View view = getActivity().getLayoutInflater().inflate(viewId, null);
        dialog.setContentView(view);
        return view;
    }

    private void showDialog() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;

        dialog.show();
        // Workaround for dialog being small
        dialog.getWindow().setLayout((6 * width)/7, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    private void setViewValues() {
        tvNickname.setText(user.getNickname());
        tvLevel.setText(String.valueOf(user.getLevel()));
        tvMessageCount.setText(String.valueOf(user.getMessageCount()));
        tvKarma.setText(String.valueOf(user.getKarma()));

        int colorResourceId = UserInfoHelper.getColorResourceInt(user.getColor());
        setMessageColor(colorResourceId);

        int avatarId = UserPreferences.getInt(Constants.USER_AVATAR);
        setAvatar(avatarId);

        tvSenderName.setText(user.getNickname());
        tvMessage.setText(fragmentContext.getResources().getString(R.string.change_color_text));
    }

    private void setAvatar(int avatarId) {
        Drawable[] avatars = getAllAvatars();
        if(avatarId < avatars.length)
            ivAvatar.setImageDrawable(avatars[avatarId]);
    }

    private void setMessageColor(int colorResourceId) {
        if(Build.VERSION.SDK_INT <= 21){
            messageChatContainer.setBackgroundColor(ContextCompat.getColor(fragmentContext, colorResourceId));
        } else{
            ViewCompat.setBackgroundTintList(messageChatContainer, ContextCompat.getColorStateList(fragmentContext, colorResourceId));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissDialog();
    }

    private void dismissDialog() {
        if(null != dialog) {
            dialog.dismiss();
        }
    }
}
