package com.alexz.pubnubchat;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.config.PubNubConfigImpl;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;

import java.util.Arrays;

/**
 * Created by Alex on 19-Sep-17.
 */

public class ChatApp extends Application {

    private static Context appContext;
    private static PubNub pubNub;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        initSenderId();
        initPubNub();
        initPreferences();
        subscribeToChannels();
    }


    private void initSenderId() {
        UserPreferences.save(Constants.SENDER_ID,
                Settings.Secure.getString(appContext.getContentResolver(), Settings.Secure.ANDROID_ID));
    }

    private void initPreferences() {
        if(UserPreferences.getStringFromPreferences(Constants.USER_NICKNAME).isEmpty())
            UserPreferences.save(Constants.USER_NICKNAME, Constants.USER_DEFAULT_NICKNAME);
    }

    @Override
    public void onTerminate() {
        UserPreferences.save(Constants.LAST_FRAGMENT_TAG, "");
        super.onTerminate();
    }

    public static Context getAppContext(){
        return appContext;
    }

    private void initPubNub() {
        PNConfiguration pnConfiguration = new PubNubConfigImpl().configure();
        pubNub = new PubNub(pnConfiguration);
    }
    public static void addPubNubCallback(SubscribeCallback callback) {
        pubNub.addListener(callback);
    }
    private void subscribeToChannels() {
        pubNub.subscribe().channels(Arrays.asList(Constants.CHANNEL)).execute();
    }

    public static PubNub getPubNub() {
        return pubNub;
    }
}
