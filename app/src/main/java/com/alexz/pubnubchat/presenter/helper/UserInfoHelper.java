package com.alexz.pubnubchat.presenter.helper;

import com.alexz.pubnubchat.R;
import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;

/**
 * Created by Alex on 21-Sep-17.
 */

public class UserInfoHelper {

    public static void incTotalMessageCount(int value){
        long totalCount = UserPreferences.getLong(Constants.USER_MESSAGE_COUNT);
        if(value > 0)
            UserPreferences.save(Constants.USER_MESSAGE_COUNT, totalCount + value);
    }

    public static long getCurrentLevel(){
        long messages = UserPreferences.getLong(Constants.USER_MESSAGE_COUNT);
        return messages > 0 ? messages / 10 : 0;
    }

    public static long getCurrentKarma() {
        long messages = UserPreferences.getLong(Constants.USER_MESSAGE_COUNT);
        return messages * 3;
    }

    public static int getColorResourceInt(Color color) {
        int colorResourceId;
        switch (color){
            case BLUE:
                colorResourceId = R.color.blue;
                break;
            case RED:
                colorResourceId = R.color.red;
                break;
            case GREEN:
                colorResourceId = R.color.green;
                break;
            case GRAY:
                colorResourceId = R.color.gray;
                break;
            case PINK:
                colorResourceId = R.color.pink;
                break;
            case BROWN:
                colorResourceId = R.color.brown;
                break;
            default:
                colorResourceId = R.color.gray;
        }
        return colorResourceId;
    }
}
