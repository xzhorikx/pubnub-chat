package com.alexz.pubnubchat.presenter.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Alex on 17-Sep-17.
 */

public class InputHelper {
    /**
     * Hiding the keyboard from the screen. Due to purely managed Android design
     * InputManager needs context and View to hide the keyboard. Therefore it's
     * more reasonable to pass the whole Activity to execute the required data
     * @param activity - current activity to hide keyboard from
     */
    public static void hideKeyboard(Activity activity){
        try {
            //Hiding virtual keyboard
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            // Find the current focused view to grab the token
            View view = activity.getCurrentFocus();

            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (null == view){
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("Error_tag", "Unable to hide keyboard on " +
                    activity.getClass().toString() + " activity");
            e.printStackTrace();
        }
    }

    /**
     * Requests focus with keyboard on specific view
     * @param view view to execute focus at
     * @param context application context
     */
    public static void requestFocusWithKeyboard(View view, Context context){
        try {
            if(view.requestFocus()) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e){
            Log.e("Error_tag","Unable to set focus on elements (Id: " + view.getId() + "). Error: " + e.getMessage());
        }
    }
}
