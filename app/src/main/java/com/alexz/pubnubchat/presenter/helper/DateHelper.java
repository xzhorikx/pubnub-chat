package com.alexz.pubnubchat.presenter.helper;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by Alex on 20-Sep-17.
 */

public class DateHelper {

    public static String getTimeFromTimetoken(long timestamp) {
        String result = ISODateTimeFormat.time().print(timestamp);
        return result.length() > 5 ? result.substring(0, result.length() - 5) : result;
    }

    public static String getDateTimeFromTimetoken(long timestamp) {
        return ISODateTimeFormat.dateTime().withZoneUTC().print(timestamp);
    }

    public static String getTimeStampUtc() {
        return DateTime.now(DateTimeZone.UTC).toString(ISODateTimeFormat.hourMinute());
    }
}
