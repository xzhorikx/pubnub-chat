package com.alexz.pubnubchat.presenter.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.alexz.pubnubchat.ChatApp;
import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.ChatMessage;
import com.alexz.pubnubchat.model.ChatMessageImpl;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Alex on 19-Sep-17.
 */

public class UserPreferences {

    public static void save(String key, Object objectToSave){
        Context context = ChatApp.getAppContext();
        if(null != objectToSave && null != context){
            try {
                Gson gson = new Gson();
                String jsonString;
                SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if(objectToSave instanceof String) {
                    jsonString = objectToSave.toString();
                } else {
                    jsonString = gson.toJson(objectToSave);
                }
                editor.putString(key, jsonString);
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Error_tag", "Can't save shared preference: " + e.toString());
            }
        } else{
            Log.e("Error_tag", "Can't save shared preference. Context or object to save is null");
        }
    }

    public static List<ChatMessage> getCachedMessages(){
        try{
            String result = getStringFromPreferences(Constants.CACHED_MESSAGES_KEY);
            Gson gson = new Gson();
            ChatMessage[] array = gson.fromJson(result, ChatMessageImpl[].class);
            return Arrays.asList(array);
        } catch (JsonSyntaxException e){
            e.printStackTrace();
            Log.e("Error_tag", "Can't get messages from shared preference: " + e.toString());
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e("Error_tag", "Can't get messages from shared preference: " + e.toString());
        }
        return new ArrayList<>();
    }

    public static String getStringFromPreferences(String key){
        try{
            Context context = ChatApp.getAppContext();
            SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFS, Context.MODE_PRIVATE);
            return sharedPreferences.getString(key, "");
        } catch (Exception e){
            e.printStackTrace();
            Log.e("Error_tag", "Can't get string from shared preference: " + e.toString());
            return "";
        }
    }

    public static int getInt(String key){
        try{
            return Integer.parseInt(getStringFromPreferences(key));
        } catch (NumberFormatException e){
            e.printStackTrace();
            Log.e("Error_tag", "Can't get int from shared preference: " + e.toString());
            return 0;
        }
    }

    public static long getLong(String key){
        try{
            return Long.parseLong(getStringFromPreferences(key));
        } catch (NumberFormatException e){
            e.printStackTrace();
            Log.e("Error_tag", "Can't get int from shared preference: " + e.toString());
            return 0;
        }
    }
}
