package com.alexz.pubnubchat.presenter.helper;

import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.ChatMessage;
import com.alexz.pubnubchat.model.ChatMessageImpl;
import com.alexz.pubnubchat.model.enums.Position;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.pubnub.api.models.consumer.history.PNHistoryItemResult;
import com.pubnub.api.models.consumer.history.PNHistoryResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 19-Sep-17.
 */

public class PubNubHelper {

    public static ChatMessage getChatMessageFromResult(PNMessageResult message, Class<? extends ChatMessage> chatClass, String userId) {
        JsonElement jsonElement = message.getMessage();
        ChatMessage chatMessage = new ChatMessageImpl();
        chatMessage.setTimestamp(message.getTimetoken());
        try{
            Gson gson = new Gson();
            chatMessage = gson.fromJson(jsonElement, chatClass);
            chatMessage.setPosition(Position.LEFT);
            // If parsing failed
            if(null == chatMessage.getSenderId()){
                chatMessage = ChatMessageImpl.buildDefault();
                chatMessage.setMessage(jsonElement.toString());
            } else if(chatMessage.getSenderId().equals(userId)){
                chatMessage.setPosition(Position.RIGHT);
            } else if(chatMessage.getSenderId().equals(Constants.SENDER_SYSTEM)){
                chatMessage.setPosition(Position.CENTER);
            }
            chatMessage.setTimestamp(message.getTimetoken());

            return chatMessage;
        } catch (Exception e){
            chatMessage.setSenderName("Unknown sender");
            chatMessage.setMessage(jsonElement.toString());
            return chatMessage;
        }
    }

    private static ChatMessage getChatMessageFromJson(JsonElement jsonElement, Class<? extends ChatMessage> chatClass){
        ChatMessage chatMessage = new ChatMessageImpl();
        try{
            Gson gson = new Gson();
            chatMessage = gson.fromJson(jsonElement, chatClass);
            // If parsing failed
            if(null == chatMessage.getSenderName()){
                chatMessage = ChatMessageImpl.buildDefault();
                chatMessage.setMessage(jsonElement.toString());
            }
            return chatMessage;
        } catch (Exception e){
            chatMessage.setSenderName("Unknown sender");
            chatMessage.setMessage(jsonElement.toString());
            return chatMessage;
        }
    }

    public static List<ChatMessage> getChatMessageListFromHistory(PNHistoryResult messageResult, Class<? extends ChatMessage> chatClass){
        List<ChatMessage> messageList = new ArrayList<>();
        if(null != messageResult) {
            List<PNHistoryItemResult> historyItems = messageResult.getMessages();

            for (PNHistoryItemResult historyItem : historyItems) {
                ChatMessage chatMessage = getChatMessageFromJson(historyItem.getEntry(), chatClass);
                chatMessage.setTimestamp(historyItem.getTimetoken());
                messageList.add(chatMessage);
            }
        }
        return messageList;
    }

}
