package com.alexz.pubnubchat.config;

import com.pubnub.api.PNConfiguration;

public interface PubNubConfig {
    /**
     * Default configuration
     * @return PubNub configuration
     */
    PNConfiguration configure();

    /**
     *
     * @param subKey subscribe key
     * @param pubKey publish key
     * @return PubNub configuration
     */
    PNConfiguration configure(String subKey, String pubKey);
    /**
     *
     * @param subKey subscribe key
     * @param pubKey publish key
     * @param secure is channel secured
     * @return PubNub configuration
     */
    PNConfiguration configure(String subKey, String pubKey, boolean secure);
}
