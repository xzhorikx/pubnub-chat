package com.alexz.pubnubchat.config;

/**
 * Created by Alex on 17-Sep-17.
 */

public class Constants {

    private static final String CHANNEL_ANTICHAT = "antichat_hackathon";
    private static final String CHANNEL_DEFAULT = "test_channel";
    public static final String CHANNEL = CHANNEL_DEFAULT;

    public static final String MESSAGE_TIMESTAMP = "timestamp";
    public static final String MESSAGE_SENDER = "senderName";
    public static final String MESSAGE_BODY = "message";
    public static final String MESSAGE_POSITION = "position";
    public static final String MESSAGE_COLOR = "color";
    public static final String MESSAGE_SENDER_ID = "senderid";

    public static final String SHARED_PREFS = "userprefs";
    public static final String CACHED_MESSAGES_KEY = "cached_messages_key";
    public static final String LIST_VIEW_INDEX = "list_view_index";
    public static final String LIST_VIEW_TOP = "list_view_top";
    public static final String SENDER_ID = "sender_id";
    public static final String HISTORY_CACHE = "history_cache";
    public static final String LAST_SAVED_TIMETOKEN = "last_timetoken";
    public static final String LAST_FRAGMENT_TAG = "last_fragment";

    public static final int PUB_NUB_MSG_LIMIT = 100;
    public static final String USER_NICKNAME = "user_nickname";
    public static final String USER_DEFAULT_NICKNAME = "New joiner";
    public static final String USER_MESSAGE_COUNT = "user_message_count";
    public static final String USER_LEVEL = "user_level";
    public static final String USER_KARMA = "user_karma";
    public static final String USER_COLOR = "user_color";
    public static final String USER_AVATAR = "user_avatar";
    public static final String SENDER_SYSTEM = "sender_system";
}
