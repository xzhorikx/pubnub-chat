package com.alexz.pubnubchat.config;

import com.pubnub.api.PNConfiguration;

/**
 * Created by Alex on 15-Sep-17.
 */

public class PubNubConfigImpl implements PubNubConfig {

    private String antichatSubKey = "sub-c-a11d1bc0-ce50-11e5-bcee-0619f8945a4f";
    private String antichatPubKey = "pub-c-8ecaf827-b81c-4d89-abf0-d669cf6da672";
    private String defaultTestSubKey = "sub-c-927193a8-99ce-11e7-b377-26d3778b8379";
    private String defaultTestPubKey = "pub-c-e6b58d03-d584-44f5-bc70-37ee5faa1dc4";

    //TODO store in config file
    private String defaultSubKey = defaultTestSubKey;
    private String defaultPubKey = defaultTestPubKey;

    @Override
    public PNConfiguration configure() {
        return configure(defaultSubKey, defaultPubKey);
    }

    @Override
    public PNConfiguration configure(String subKey, String pubKey) {
        return configure(defaultSubKey, defaultPubKey, false);
    }

    @Override
    public PNConfiguration configure(String subKey, String pubKey, boolean secure) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(subKey);
        pnConfiguration.setPublishKey(pubKey);
        pnConfiguration.setSecure(secure);
        return pnConfiguration;
    }
}
