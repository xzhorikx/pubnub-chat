package com.alexz.pubnubchat.model;


import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.model.enums.Position;

public interface ChatMessage {
    void setPosition(Position position);
    void setMessage(String message);
    void setSenderName(String sender);
    void setTimestamp(long timestamp);
    void setColorBg(Color colorBg);
    void setSenderId(String senderId);

    Position getPosition();
    String getMessage();
    String getSenderName();
    long getTimestamp();
    Color getColorBg();
    String getSenderId();
}
