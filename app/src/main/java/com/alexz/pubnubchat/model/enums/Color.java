package com.alexz.pubnubchat.model.enums;

public enum Color {
    BLUE(1),
    RED(2),
    GREEN(3),
    GRAY(4),
    PINK(5),
    BROWN(6),
    DEFAULT(0);
    int id;

    Color(int i){id = i;}

    public int getId(){return id;}

    public boolean isEmpty(){return this.equals(Color.DEFAULT);}

    public boolean compare(int id){return this.id == id;}

    public static Color getValue(int id){
        Color[] colors = Color.values();
        for (int i = 0; i < colors.length; i++) {
            if(colors[i].compare(id))
                return colors[i];
        }
        return Color.DEFAULT;
    }
}
