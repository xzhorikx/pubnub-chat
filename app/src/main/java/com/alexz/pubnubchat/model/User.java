package com.alexz.pubnubchat.model;

import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.presenter.helper.UserInfoHelper;
import com.alexz.pubnubchat.presenter.preferences.UserPreferences;

/**
 * Created by Alex on 21-Sep-17.
 */

public class User {

    private String nickname;
    private long messageCount;
    private long level;
    private long karma;
    private Color color;

    private User() {
    }

    public static User build(){
        User user = new User();
        user.setNickname(UserPreferences.getStringFromPreferences(Constants.USER_NICKNAME));
        long messages = UserPreferences.getLong(Constants.USER_MESSAGE_COUNT);
        user.setMessageCount(messages);
        user.setLevel(UserInfoHelper.getCurrentLevel());
        user.setKarma(UserInfoHelper.getCurrentKarma());
        int colorId = UserPreferences.getInt(Constants.USER_COLOR);
        user.setColor(Color.getValue(colorId));
        return user;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(long messageCount) {
        this.messageCount = messageCount;
    }

    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    public long getKarma() {
        return karma;
    }

    public void setKarma(long karma) {
        this.karma = karma;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
