package com.alexz.pubnubchat.model.enums;

/**
 * Created by Alex on 21-Sep-17.
 */

public enum ChatMessageSource {
    USER, SYSTEM
}
