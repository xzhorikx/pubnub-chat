package com.alexz.pubnubchat.model.enums;

/**
 * Created by Alex on 17-Sep-17.
 */

public enum Position {
    LEFT, RIGHT, CENTER;
}
