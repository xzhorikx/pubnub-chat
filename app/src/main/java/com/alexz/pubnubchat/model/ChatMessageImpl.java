package com.alexz.pubnubchat.model;

import com.alexz.pubnubchat.config.Constants;
import com.alexz.pubnubchat.model.enums.Color;
import com.alexz.pubnubchat.model.enums.Position;
import com.google.gson.annotations.SerializedName;

public class ChatMessageImpl implements ChatMessage {

    @SerializedName(Constants.MESSAGE_POSITION)
    private Position position;
    @SerializedName(Constants.MESSAGE_SENDER)
    private String senderName;
    @SerializedName(Constants.MESSAGE_SENDER_ID)
    private String senderId;
    @SerializedName(Constants.MESSAGE_BODY)
    private String message;
    @SerializedName(Constants.MESSAGE_TIMESTAMP)
    private Long timestamp;
    @SerializedName(Constants.MESSAGE_COLOR)
    private Color colorBg;

    public ChatMessageImpl() {
        this.timestamp = Long.valueOf(0);
        this.position = Position.LEFT;
        this.colorBg = Color.GRAY;
    }

    public static ChatMessage buildDefault(){
        ChatMessage chatMessage = new ChatMessageImpl();
        chatMessage.setSenderName("Unknown");
        chatMessage.setMessage("");
        return chatMessage;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public Color getColorBg() {
        return colorBg;
    }

    public void setColorBg(Color colorBg) {
        this.colorBg = colorBg;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
